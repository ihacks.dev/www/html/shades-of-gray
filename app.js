// Imports
import {
	generate,
	preview,
	stringify,
	parse
} from "./generator.js";

/**
 * The container element.
 * @type {HTMLDivElement}
 */
const containerEl = document.querySelector("#container");

/** @param {HTMLDivElement} el */
const clear = el =>
{
	for (let child of el.children) child.remove();
}

/**
 * Generate a new canvas.
 */
const generateClickEvt = () =>
{
	clear(containerEl);
	const image = generate(256);
	const canvas = preview(image);
	containerEl.appendChild(canvas);
	const str = stringify(image);
	console.log(str.length);
};

generateClickEvt()
