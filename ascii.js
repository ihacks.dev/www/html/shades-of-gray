
/**
 * A zero bigint.
 * @type {bigint}
 */
export const zero = 0n;

/**
 * A one bigint.
 * @type {bigint}
 */
export const one = 1n;

/**
 * The byte length.
 * @type {bigint}
 */
export const byteLength = 8n;

/**
 * The max number in Base256 known as ASCII.
 * @type {bigint}
 */
export const byte = 255n;

/**
 * Encode a big integer as ASCII text.
 * @param {bigint} n The big integer.
 * @returns {string}
 */
export const encode = n =>
{
	let str = "";
	while (n > zero)
	{
		str += String.fromCharCode(Number(n & byte));
		n = n >> byteLength;
	}
	return str;
};

/**
 * Decode an ASCII string to a big integer.
 * @param {string} str The string to decode.
 * @returns {bigint}
 */
export const decode = str =>
{
	let n = 0n;
	for (let i = str.length - 1; i > 0; i--)
		n = n | ( BigInt(str.charCodeAt(i)) << ( BigInt(i) * byteLength ));
	return n;
};
