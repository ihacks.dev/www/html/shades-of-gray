// Imports
import {
	byte,
	byteLength,
	zero,
	one,
	encode,
	decode
} from "./ascii.js";

/**
 * Get a random scale of gray.
 * @returns {bigint}
 */
export const getRandomScale = () => BigInt(Math.floor(Math.random() * 255));

/**
 * Generate a new image with scales of gray.
 * @param {bigint | number} size The square size of the image.
 * @returns {RawByteImage}
 */
export const generate = size =>
{
	let data = zero;
	const squared = BigInt(size * size);
	for (let i = zero; i < squared; i += one)
		data = data | ( getRandomScale() << (BigInt(i) * byteLength) );
	return [
		BigInt(size),
		data
	];
}

/**
 * Encode the image to a JSON string.
 * @param {RawByteImage} image The image.
 */
export const stringify = image => JSON.stringify(image.map(encode));

/**
 * Parse a byte image back to the size and bytes.
 * @param {EncodedByteImage} encoded The encoded byte image.
 * @returns {RawByteImage}
 */
export const parse = encoded => JSON.parse(encoded).map(decode);

/**
 * Create an image preview.
 * @param {RawByteImage} image The byte image.
 * @returns {HTMLCanvasElement}
 */
export const preview = image =>
{
	const size = Number(image[0]);
	let bytes = image[1];
	
	/** @type {HTMLCanvasElement} */
	const canvas = document.createElement("canvas");
	canvas.width = Number(size);
	canvas.height = canvas.width;
	const ctx = canvas.getContext("2d");
	
	for (let y = 0; y < size; y++)
		for (let x = 0; x < size; x++)
		{
			const b = bytes & byte;
			bytes = bytes >> byteLength;
			ctx.fillStyle = `#${b.toString(16).padStart(2, "0").repeat(3)}`;
			ctx.fillRect(x, y, x + 1, y + 1);
		}
	
	return canvas;
};

/**
 * An encoded type image.
 * @typedef {string} EncodedByteImage
 */

/**
 * A raw byte image.
 * @typedef {[ bigint, bigint ]} RawByteImage
 */
